#! /usr/bin/python3
"""
Module to control touchpad in absolute mode
"""

import asyncio
from collections import namedtuple
import ctypes
import ctypes.util
from typing import Optional, Tuple, Union
import argparse
import evdev
from evdev import InputDevice
from evdev.ecodes import EV_ABS, ABS_X, ABS_Y
from Xlib import display
from copy import deepcopy


# shortcut support
import pyatspi
import threading


Point = namedtuple("Point", "x y")

XDO_PATH = ctypes.util.find_library("xdo")
if XDO_PATH is None:
    raise ImportError("c library xdo not found")
LIBXDO = ctypes.CDLL(XDO_PATH)



class Rectangle:

    def __init__(self, width: int, height: int, x: int = 0, y: int = 0):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def fix(self, x: int, y: int) -> Tuple[int, int]:
        """
        @brief fix out of bound coordinate by setting them to min or max
        :param x: x coordinate
        :param y: y coordinate
        :returns: (x,y)
        """
        resX = x
        resY = y
        if x < self.x:
            resX = self.x
        elif x > self.x + self.width:
            resX = self.x + self.width - 1
        if y < self.y:
            resY = self.y
        elif y > self.y + self.height:
            resY = self.y + self.height - 1

        return (resX, resY)

    def zoom(self, ref_pt: Point, scale: int = 2):
        """
        @brief zoom from point 'ref_point' with a factor of scale.
        After zoom, 'ref_point' is on the same absolute and relative to new screen
        position
        :param ref_point: point of reference. given in absolute coordinate
        :param scale: scale of zoom. Use value smaller than 1 to zoom in
        """
        print(self)
        print(ref_pt)
        rel_pt = Point(x=ref_pt.x-self.x, y=ref_pt.y-self.y)

        self.width = int(self.width/scale)
        self.height = int(self.height/scale)
        self.x = ref_pt.x-int(rel_pt.x/scale)
        self.y = ref_pt.y-int(rel_pt.y/scale)
        print(self)
    
    def __str__(self):
        return "Rectangle ({},{})[{}, {}]".format(self.x, self.y, self.width, self.height)

    @property
    def min_x(self):
        return self.x

    @property
    def min_y(self):
        return self.y

    @property
    def max_x(self):
        return self.x + self.width

    @property
    def max_y(self):
        return self.y + self.height


def get_screen_size() -> Rectangle:
    screen = display.Display().screen()
    return Rectangle(width=screen.width_in_pixels, height=screen.height_in_pixels)


class Pad:
    """
    <node name='/com/tactos/TouchPad'>
        <interface name='com.tactos.TouchPad'>
            <method name='zoomFromPointer'>
                <arg type='d' name='scale'/>
            </method>
            <method name='zoomArea'>
                <arg type='i' name='direction'/> <!-- 0 : no zoom ; 1 : zoom in ; 2 : zoom out-->
                <arg type='i' name='area'>
                <!-- 0 : zoom out on the area containing the previous one
                      1 | 2   1 to 4 : quarter of previous area to choose
                     ---|---
                      3 | 4  -->
            </method>
            <property type='((ii)(ii))' name='screen' access='readwrite'>
                <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
            </property>
            <property type='(ii)' name='Matrix' access='readwrite'>
                <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
            </property>
        </interface>
    </node>
    """

    def __init__(
        self,
        dev: Union[InputDevice, str],
        bounding: Optional[Rectangle] = None,
        x=0,
        y=0,
        scale=0.05,
        scr_size: Optional[Rectangle] = None,
    ):

        self.current_x = x  # current pad detected position
        self.current_y = y  # current pad detected position
        self._scale = scale  # small zoom to avoid using border of the touchpad
        self._update_pos = False  # update the mouse position
        self._xdo = LIBXDO.xdo_new(None)
        
        # initial define screen size
        if scr_size is None:
            self._full_screen = get_screen_size()
        else:
            self._full_screen = scr_size
        # define current screen size
        self._screen = deepcopy(self._full_screen)
        self.focus_position = (0,)

        # define and open device
        if isinstance(dev, str):
            dev = get_device_from_name(dev)
        if isinstance(dev, InputDevice):
            self.device = dev
        else:
            raise TypeError("Device no found")

        # define pad bounding
        if bounding is None:
            self._bound = calibration(self.device, scale)
        else:
            self._bound: Rectangle = bounding

    def __str__(self) -> str:
        return "Pad(\n\tdev={},\n\t position = [{},{}] \n\t bounding : {}\n\t screen : {} )".format(
            self.device.path, self.current_x, self.current_y, self._bound, self._screen
        )
    
    @property
    def focus_position(self): return self._focus_position

    @focus_position.setter
    def focus_position(self, pos):
        """
        @brief set mapping of the pad to knowm area in the screen.
        :param pos: p_pos: tuple containing the successive position in the grid. Each
        level correspond to a division of the screen in 4 area:
        ```
           1 | 2
          ---|---
           3 | 4
        ```
        """
        self._screen = deepcopy(self._full_screen)
        
        if pos[0] == 0:  # set mapping to full screen
            return
            
        for el in pos:
            print(el)
            self._screen.width = int(self._screen.width/2)
            self._screen.height = int(self._screen.height/2)
            if el == 1:  # origin stay the same
                pass
            elif el == 2:
                self._screen.x = self._screen.x + self._screen.width
            elif el == 3:
                self._screen.y = self._screen.y + self._screen.height
            elif el == 4:
                self._screen.x = self._screen.x + self._screen.width
                self._screen.y = self._screen.y + self._screen.height
                
    def zoom_from_pointer(self, x: int = None, y: int = None, scale=2):
        x = x or self.current_x
        y = y or self.current_y        
        self._screen.zoom(self.mouse_position(), scale)

    def update_bound(self, scale: float) -> None:
        """
        @brief update pad detection area
        :param scale: new optional scale
        """
        self._scale = scale
        self._bound: Rectangle = calibration(self.device, scale)

    def run_capture_loop(self) -> None:
        """
        @brief run asyncio capture loop should continue until stop_capture_loop
        is called
        """
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.touchpad_cllbk())

    # pylint: disable=invalid-name
    def mouse_position(self) -> Tuple[int, int]:
        """
        @brief calculate mouse position according to pad values, pad bounding and
        screen size
        :returns: (x,y) position of the mouse
        """
        (fixX, fixY) = self._bound.fix(self.current_x, self.current_y)
        
        x = self._screen.min_x + int(
            (fixX - self._bound.min_x)
            * self._screen.width
            / (self._bound.width)
        )
        y = self._screen.min_y + int(
            (fixY - self._bound.min_y)
            * self._screen.height
            / (self._bound.height)
        )

        return Point(x=x, y=y)

    async def touchpad_cllbk(self) -> None:
        """
        @brief get event from touchpad. After receving ABS_X and ABS_Y events,
        move mouse pointer

        @details use c library libxdi for good performances
        """
        async for ev in self.device.async_read_loop():
            if ev.type == EV_ABS:
                if ev.code == ABS_X:
                    self.current_x = ev.value
                elif ev.code == ABS_Y:
                    self.current_y = ev.value
                if self._update_pos is True:
                    x, y = self.mouse_position()
                    LIBXDO.xdo_move_mouse(self._xdo, x, y, 0)
                    #print('mouse in position', x, y)
                    self._update_pos = False
                else:
                    self._update_pos = True


def get_device_from_name(name: Union[str, InputDevice]) -> Optional[InputDevice]:
    """
    @brief open device from str description.

    @detail List all event from '/dev/input/event*' and look for a description
    containing :param name:. Return the first found or None if nothing is found

    :param name: part of event description
    """
    devices = [InputDevice(path) for path in evdev.list_devices()]
    for device in devices:
        if name in device.name.lower():
            return device
    return None


def calibration(pad: InputDevice, scale=0.1) -> Rectangle:
    """
    @brief define pad bounding values

    @details reduce pad size with :param scale: factor.
    :param pad: InputEvent associated to the TouchPad
    :param scale: Scale of the pad to be ignored default 0.1
    :return: Tuple containing range of width and height
    """
    caps = pad.capabilities(absinfo=True)
    x_info = caps[EV_ABS][ABS_X][1]
    y_info = caps[EV_ABS][ABS_Y][1]
    x_pts = int(scale * x_info.max)
    y_pts = int(scale * y_info.max)

    width = x_info.max - x_info.min - 2*x_pts
    height = y_info.max - y_info.min - 2*y_pts
    return Rectangle(width=width, height=height, x=x_info.min + x_pts, y=y_info.min + y_pts)


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser(description="set absolute touchpad mode")
    PARSER.add_argument("--name", help="part name of the event")

    ARGS = PARSER.parse_args()

    NAME = None
    if ARGS.name:
        NAME = ARGS.name
    else:
        NAME = "touchpad"

    PAD = Pad(NAME)
    try:
        print("Sarting touchpad absolute mode... press Ctrl+c to quit")
        PAD.run_capture_loop()
    except KeyboardInterrupt:
        LOOP = asyncio.get_event_loop()
        LOOP.stop()

##### <div style="text-align: right"> Yann Ladeve </div>  

#  <center>Rapport TX-6108 </center>

## Plan
1. Objectif
2. Points de recherche
	* Récupération des coordonnées absolues du doigt sur le touchpad
	* Gestion des périphériques grâce à un pilote
	* Utilisation d'un programme en C++
	* Réécriture du programme evtest.c
	* Création d'un utilisateur pouvant utiliser evtest.c sans sudo
3. Points d'amélioration
4. Conclusion


## Objectif

<div style="text-align: justify">
Pour rappel le projet **Tactos** a pour but de permettre aux aveugles de percevoir les écrans d'ordinateurs. L'objectif de cette TX est de pouvoir utiliser le pavé tactile des ordinateurs portables de manière absolue. C'est à dire que la position de la souris sur l'écran corresponde à la position du doigt sur le pavé tactile, ce qui n'est pas le cas par défaut sur les pc portables.
</div>

## Points de recherche

#### **1. Récupération des coordonnées absolues du doigt sur le touchpad**

Pour pouvoir mettre le touchpad en mode absolu (indépendamment de la solution utilisée), il faut que celui-ci soit capable de renvoyer les coordonnées absolues.

Il existe différents types de touchpads en fonction des pc, les trois principaux sont : synaptics, elantech, alps. Pour l'instant seuls synaptics et elan ont été testés et il n'y a pas de problème. De plus alps ne semble pas poser de problème non plus selon sa documentation.

Il peut être intéressant de récupérer ces coordonnées. Pour ce faire il faut utiliser **evtest** (`apt-get install evtest`). evtest est un programme qui affiche dans le terminal les évènements générés par les périphériques.

Il faut utiliser la commande : `sudo evtest /dev/input/event<i>`, avec i le numéro de l'évènement associé au périphérique.
Pour connaître l'évènement :

	cat /proc/bus/input/devices

	I: Bus=0011 Vendor=0002 Product=000e Version=0000
	N: Name="ETPS/2 Elantech Touchpad"
	P: Phys=isa0060/serio1/input0
	S: Sysfs=/devices/platform/i8042/serio1/input/input2
	U: Uniq=
	H: Handlers=mouse0 event1
	B: PROP=5
	B: EV=b
	B: KEY=e420 10000 0 0 0 0
	B: ABS=661800011000003

Ici l'event associé au touchpad est le numéro 1.

	sudo evtest /dev/input/event 1
	Input driver version is 1.0.1
	Input device ID: bus 0x11 vendor 0x2 product 0xe version 0x0
	Input device name: "ETPS/2 Elantech Touchpad"
	Supported events:
	  Event type 0 (EV_SYN)
	  Event type 1 (EV_KEY)
	    Event code 272 (BTN_LEFT)
	    Event code 325 (BTN_TOOL_FINGER)
	    Event code 330 (BTN_TOUCH)
	    Event code 333 (BTN_TOOL_DOUBLETAP)
	    Event code 334 (BTN_TOOL_TRIPLETAP)
	    Event code 335 (BTN_TOOL_QUADTAP)
	  Event type 3 (EV_ABS)
	    Event code 0 (ABS_X)
	      Value   3225
	      Min        0
	      Max     3260
	      Resolution      32
	    Event code 1 (ABS_Y)
	      Value   1756
	      Min        0
	      Max     2282
	      Resolution      31
	...
	Event: time 1556118394.237848, type 3 (EV_ABS), code 53 (ABS_MT_POSITION_X), value 1651
	Event: time 1556118394.237848, type 3 (EV_ABS), code 54 (ABS_MT_POSITION_Y), value 1793
	Event: time 1556118394.237848, type 3 (EV_ABS), code 58 (ABS_MT_PRESSURE), value 47
	Event: time 1556118394.237848, type 3 (EV_ABS), code 0 (ABS_X), value 1651
	Event: time 1556118394.237848, type 3 (EV_ABS), code 1 (ABS_Y), value 1793
	Event: time 1556118394.237848, type 3 (EV_ABS), code 24 (ABS_PRESSURE), value 47

On peut voir les coordonnées absolues du doigt avec les évènements ABS_X et  ABS_Y.

#### **2. Gestion des périphériques grâce à un pilote**

Pour gérer les périphériques de l'ordinateur (clavier, pavé tactile, souris ...), il faut utiliser un pilote.

Actuellement trois pilotes peuvent être utilisés :
* evdev
* libinput
* synaptics

Chacun de ces pilotes propose des options communes mais aussi des options spécifiques.

  * **Informations d'utilisation** :
Pour savoir quel périphérique est installé, on peut utiliser **xinput**:

		apt-get install xinput

	Par défaut un des trois pilotes est installé, pour savoir lequel, faire :

		xinput list

		Virtual core pointer                    	id=2	[master pointer  (3)]
		⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
		⎜   ↳ ELAN Touchscreen                        	id=10	[slave  pointer  (2)]
		⎜   ↳ Logitech USB Receiver                   	id=12	[slave  pointer  (2)]
		⎜   ↳ Logitech USB Receiver                   	id=13	[slave  pointer  (2)]
		⎜   ↳ ETPS/2 Elantech Touchpad                	id=15	[slave  pointer  (2)]
		⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
		    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
		    ↳ Power Button                            	id=6	[slave  keyboard (3)]
		    ↳ Video Bus                               	id=7	[slave  keyboard (3)]
		    ↳ Video Bus                               	id=8	[slave  keyboard (3)]
		    ↳ Sleep Button                            	id=9	[slave  keyboard (3)]
		    ↳ HD WebCam                               	id=11	[slave  keyboard (3)]
		    ↳ AT Translated Set 2 keyboard            	id=14	[slave  keyboard (3)]
		    ↳ Acer WMI hotkeys                        	id=16	[slave  keyboard (3)]

	Puis faire :  `xinput list-props <id>`
	Ici l'id est 15.


		Device 'ETPS/2 Elantech Touchpad':
		Device Enabled (143):	1
		Coordinate Transformation Matrix (145):	1.000000,
		0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 1.000000
		Device Accel Profile (279):	0
		Device Accel Constant Deceleration (280):	1.000000
		Device Accel Adaptive Deceleration (281):	1.000000
		Device Accel Velocity Scaling (282):	10.000000
		Device Product ID (266):	2, 14
		Device Node (267):	"/dev/input/event1"
		Evdev Axis Inversion (283):	0, 0
		Evdev Axis Calibration (284):	<no items>
		Evdev Axes Swap (285):	0
		Axis Labels (286):	"Abs MT Position X" (275),
		"Abs MT Position Y" (276), "Abs MT Pressure" (312), "Abs Tool Width" (311),
		"Abs MT Touch Major" (272), "None" (0), "None" (0), "None" (0)
		Button Labels (287):	"Button Left" (146), "Button Unknown" (269),
		"Button Unknown" (269), "Button Wheel Up" (149), "Button Wheel Down" (150)
		Evdev Scrolling Distance (288):	0, 0, 0
		Evdev Middle Button Emulation (289):	0
		Evdev Middle Button Timeout (290):	50
		Evdev Middle Button Button (291):	2
		Evdev Third Button Emulation (292):	0
		Evdev Third Button Emulation Timeout (293):	1000
		Evdev Third Button Emulation Button (294):	3
		Evdev Third Button Emulation Threshold (295):	20
		Evdev Wheel Emulation (296):	0
		Evdev Wheel Emulation Axes (297):	0, 0, 4, 5
		Evdev Wheel Emulation Inertia (298):	10
		Evdev Wheel Emulation Timeout (299):	200
		Evdev Wheel Emulation Button (300):	4
		Evdev Drag Lock Buttons (301):	0

On peut voir qu'ici le pilote est **evdev**.

Lors de l'installation de pilotes des fichiers .conf sont ajoutés. Ces fichiers se trouvent dans le dossier :
`/usr/share/X11/xorg.conf.d/`

Voici un exemple :

	10-amdgpu.conf 10-quirks.conf 40-libinput.conf
	70-synaptics.conf 70-wacom.conf 80-evdev.conf

Ici, il y a un fichier .conf pour chaque pilote mais seulement un seul est pris en compte. L'ensemble de ces fichiers sont compilés et c'est le fichier avec le nombre le plus grand qui est considéré.
Si on veut par exemple repasser sur synaptics, il suffit de faire :

	 mv 80-evdev.conf 10-evdev.conf

 *  **Résultats** :  
Pour le moment aucune solution n'a été trouvée avec ces pilotes. **evdev** semble être le plus prometteur avec notamment l'option `"Option" "Mode" "Absolute|Relative"` qui permet de passer un périphérique en mode absolu ou relatif s'il en a la possibilité.
Mais un autre problème vient s'ajouter avec **evdev** qui si on l'utilise, bloque la souris. Aucune explication n'a été trouvée pour ce problème.

#### **3. Utilisation d'un programme en C++**

L'autre alternative trouvée est avec un programme qui fait le nécessaire pour avoir le touchpad en absolu.
```c++
    int main(){
	  unsigned int max_x = 0, max_y = 0;
	  unsigned int x,y;
	  std::string command = "xdotool mousemove 1 1";
	  std::string result;

	  GetStdoutFromCommand(command);
	  while(1){
	    command = "xdotool getmouselocation";
	    result = GetStdoutFromCommand(command);
	    std::sscanf(result.c_str(),"x:%d y:%d",&x,&y);
			/* Récupère la dimension de l\'écran */
	    if(x>max_x || y>max_y){
	      max_x = x;
	      max_y = y;
	      command = "xdotool mousemove "+std::to_string(++x)+" "+std::to_string(++y);
	      GetStdoutFromCommand(command);
	    }
	    else
	      break;
	  }

	  unsigned int max_x_tp = 3260,max_y_tp = 2282;

	  int i[3]={3,0,0};
	  pid_t pid = fork();
	  if(pid==0){
			/* Ecrit dans un fichier txt les données */
	    std::string event = GetEvent();
	    command = "evtest /dev/input/event"+event+" | egrep -i 'code 0|code 1' > data.txt";
	    system(command.c_str());
	  }
	  else{
			/* Lit le fichier texte */
	    sleep(1);
	    while(1)
	      readData(i,max_x,max_y,max_x_tp,max_y_tp);
	  }
	  return 0;
}

```
Pour ce programme **evtest** et **xdotool** sont utilisés. xdotool permet de déplacer la souris aux coordonnées souhaitées. Cependant ces coordonnées ne sont pas les mêmes que celles du touchpad.
La première partie du programme permet d'avoir les coordonnées max x et y de l'écran avec l'utilisation de  :

	 xdotool mousemove x y
Un balayage de l'écran est fait jusqu'à temps que les valeurs ne changent plus.
La fonction :
 ```c++
	GetStdoutFromCommand()
```
permet de récupérer un string de ce qui est affiché dans le terminal.
Ensuite les variables :
```c++
unsigned int max_x_tp,max_y_tp
```
sont les coordonnées max du touchpad.

Pour la deuxième partie du programme un fork est utilisé pour paralléliser les actions: le processus père lance **evtest** (vu dans la partie 1) et enregistre les données dans un fichier texte, le processus fils lit ce fichier texte et fait bouger la souris avec la fonction :

```c++
void readData(int *n,unsigned int max_x,unsigned int max_y,
unsigned int max_x_tp,unsigned int max_y_tp){
  std::string command,result;
  char *p,*ptr;
  std::ifstream myfile("data.txt");
  if(myfile.is_open()){
    if(n[0]==0){
      getline(myfile,result);
      getline(myfile,result);
      n[0] = 3;
    }
    else
      for(int j=0;j<n[0];j++)
        getline(myfile,result);
    while(!myfile.eof()){
      getline(myfile,result);
      n[0]++;
      p = new char[result.length()+1];
      std::strcpy(p,result.c_str());
      ptr = strrchr(p,' ');
      delete[] p;
			/* Ne récupère que la valeur dans la ligne */
      if(result.find("ABS_X")!=std::string::npos)
        std::sscanf(ptr," %d",&n[1]);
      if(result.find("ABS_Y")!=std::string::npos)
        std::sscanf(ptr," %d",&n[2]);
      command = "xdotool mousemove "+
			/* Fait la conversion */
      std::to_string((double(max_x)*double(n[1]))/double(max_x_tp))+
      " "+std::to_string((double(max_y)*double(n[2]))/double(max_y_tp));
      system(command.c_str());
    }
    myfile.close();
  }
}
```

Cette fonction lit le fichier texte en récupérant seulement les valeurs x,y du touchpad. Pour avoir les valeurs correspondantes sur l'écran un produit en croix est réalisé. Comme le fichier se met à jour constamment, un test est fait pour savoir où reprendre dans le fichier à chaque réouverture de celui-ci.

* **Résultat**:
La souris suit le mouvement du doigt avec un délai, la précision n'est pas optimale et le programme est gourmand en ressources. De plus, si on touche juste le touchpad, la souris ne se déplace pas directement dessus.

Abandon de ce programme trop peu performant.

#### **4. Réécriture du programme evtest.c**

Suite à l'abandon du programme précédent, il a fallu trouver une autre solution de programmation. Le programme evtest permet de récupérer ces coordonnées absolues de manière très efficace. Il a été donc décidé de modifier directement ce programme afin que les informations récupérées soient directement utilisées pour modifier la position de la souris.

3 structures sont mises en place **screen_t** qui permet de récupérer les dimensions de l'écran, **range_axes** qui permet de savoir les valeurs min et max que peuvent prendre les coordonnées x y et finalement **mouse_position** qui récupère les coordonnées de la souris sur l'écran.
```c
	/*! Dimension de l'écran */
	typedef struct screen_t{
		float width;
		float height;
	}screen_t;

	/*! L'intervalle de valeur que peut prendre en x et en y
	que peut détecter le touchpad */
	typedef struct range_axes{
		float min;
		float max;
	}range_axes;

	/*!  Les coordonnées de la position courante du doigt sur le touchpad*/
	typedef struct mouse_position{
		unsigned int width;
		unsigned int height;
		unsigned int set;
	}mouse_position;
```


```c
	/*!
	 * \fn void calibrage()
	 * \brief Détermination des dimensions de l'écran
	 * \return void
	 */
	void calibrage(){
		Display *disp;
		Window root;
		Screen screen;

		/* Connexion au serveur X */
		disp = XOpenDisplay(0);
		DefaultScreen(disp);
		root = DefaultRootWindow(disp);

		/* Récupération des dimensions de l'écran */
		XWindowAttributes w_attr;
		XGetWindowAttributes(disp,root,&w_attr);
		screen = *w_attr.screen;

		screen_size.width = screen.width;
		screen_size.height = screen.height;

		XCloseDisplay(disp);
	}

```

La fonction `calibrage()` va demander au serveur X les dimensions de l'écran et les stocke dans la variable screen_size.


```c
/**
	 * \fn read_absdata
	 * \brief Détermination de l'intervalle de valeur, en absolu,
	 * 		  	que peut renvoyer le système pour la position du doigt
	 * 		  	sur le touchpad
	 * \param fd descripteur
	 * \param axis int
	 * \param type int
	 * \return void
	 */
	void read_absdata(int fd, int axis, int type){
		int abs[6] = {0};
		int k;
		/* Demande de récuperer les inversions concernant x et y en absolu*/
		ioctl(fd, EVIOCGABS(axis), abs);
		for (k = 0; k < 6; k++){
			if ((k < 3) || abs[k]){
				/*Vérifie que la valeur correspond au Min */
				if (strcmp(absval[k],"Min")==0){
					/* Vérifie que la valeur Min correspond à l'axe X */
					if(strcmp(codename(type,axis),"ABS_X")==0){
						width_range.min = (1+SCALE)*abs[k];
						if(width_range.min==0)
						 	width_range.min = DEFAULT_SCALE;
					}
					/*! Vérifie que la valeur Min correspond à l'axe Y */
					if(strcmp(codename(type,axis),"ABS_Y")==0){
						height_range.min = (1+SCALE)*abs[k];
						if(height_range.min==0)
							height_range.min = DEFAULT_SCALE;
					}
				}
				/* Vérifie que la valeur correspond au Max */
				if (strcmp(absval[k],"Max")==0){
					/* Vérifie que la valeur Min correspond à l'axe X */
					if(strcmp(codename(type,axis),"ABS_X")==0)
						width_range.max = (1-SCALE)*abs[k];
					/* Vérifie que la valeur Min correspond à l'axe Y */
					if(strcmp(codename(type,axis),"ABS_Y")==0)
						height_range.max = (1-SCALE)*abs[k];
				}
			}
		}
	}

```

La fonction `read_absdata()` va récupérer les valeurs min et max que peut prendre la position du doigt sur le touchpad. Un paramètre d'échelle est rajouté. En effet il a été remarqué qu'à l'utilisation une échelle 1:1 rendait les bords de l'écran difficilement accessible à cause de la taille du doigt, cette modification d'échelle permet de faciliter le déplacement de la souris sur l'écran.

```c
	/*!
	 * \fn int read_device_info
	 * Récupère toutes les évènements qui peuvent être détectés
	 * (paramétré pour que seul les informations liées au
	 * touchpad soient envoyées)
	 */
	int read_device_info(int fd){
		unsigned int type, code;
		int version;
		unsigned short id[4];
		char name[256] = "Unknown";
		unsigned long bit[EV_MAX][NBITS(KEY_MAX)];

		if (ioctl(fd, EVIOCGVERSION, &version)) {
			perror("evtest: can't get version");
			return 1;
		}
		/* Récupère le nom du périphérique */
		ioctl(fd, EVIOCGID, id);
		ioctl(fd, EVIOCGNAME(sizeof(name)), name);
		memset(bit, 0, sizeof(bit));
		/* Récupère les évènements supportés par le périphérique */
		ioctl(fd, EVIOCGBIT(0, EV_MAX), bit[0]);

		for (type = 0; type < EV_MAX; type++) {
			if (test_bit(type, bit[0]) && type != EV_REP) {
					if (type == EV_SYN) continue;
					ioctl(fd, EVIOCGBIT(type, KEY_MAX), bit[type]);
					for (code = 0; code < KEY_MAX; code++){
						if (test_bit(code, bit[type])) {
							/* Test pour lancer print_absdata seulement pour le
							touchpad*/
							if (strcmp(codename(type,code),"ABS_X")==0
								|| strcmp(codename(type,code),"ABS_Y")
								==0){
								if (type == EV_ABS)
									read_absdata(fd, code, type);
							}
						}
					}
				}
			}
		return 0;
	}

```
Cette fonction fait tous les appels nécessaires pour récupérer les informations concernant seulement les events du type ABS_X ou ABS_Y.

```c
	/*! Récupère l'évènement associé au TouchPad */
	char* GetEvent(){
		char *tmp = malloc(sizeof(char)*2);
		GetStdoutFromCommand(tmp,"egrep -i -n4 'TouchPad'  /proc/bus/input/devices
			| egrep -i -o 'event[[:digit:]]{1,}' | egrep -i -o '[[:digit:]]{1,}'");
		return tmp;
	}
```

La fonction `GetEvent()` rècupère l'évènement associé au Touchpad. Cette valeur n'est pas fixe, on peut notamment la voir en faisant `xinput list`

```c

	/*!
	 * \fn setMouse
	 * Positionne la souris en coordonnées absolues
	 */
	void setMouse(unsigned int code,unsigned int type, unsigned int value){
		char buffer1[15],buffer2[15];
		/* Regarde à quel axe est associé value */
		if(strcmp(codename(type,code),"ABS_X") == 0){
			new_position.width = value;
			new_position.set++;
		}
		if (strcmp(codename(type,code),"ABS_Y") == 0){
			new_position.height = value;
			new_position.set++;
		}
		/* Vérification que les coordonnées X et Y ont bien été données
		 *  Fait le produit en croix pour adapter la valeur à l'écran
		 */
		if(new_position.set == 2){
			/* Bloque la souris au bord de l'écran si la valeur de width
				récupérée dépasse du touchpad redimensionné*/
			if(new_position.width>width_range.max)
				gcvt(screen_size.width-1,6,buffer1);
			else if(new_position.width<width_range.min)
				gcvt(0,6,buffer1);
			else
				gcvt((new_position.width-width_range.min)*
					screen_size.width/
					(width_range.max-width_range.min),6,buffer1);

			/* Bloque la souris au bord de l'écran si la valeur de heigth
				récupérée dépasse du touchpad redimensionné*/
			if(new_position.height>height_range.max)
				gcvt(screen_size.height-1,6,buffer2);
			else if(new_position.height<height_range.min)
					gcvt(0,6,buffer2);
			else
				gcvt((new_position.height-height_range.min)*
					screen_size.height/
					(height_range.max-height_range.min),6,buffer2);

			char *buf3 = malloc(strlen(buffer1)+strlen(buffer2)+18);
			/* Lance xdotool dans le shell avec bonne commande */
			strcpy(buf3,"xdotool mousemove ");
			strcat(buf3,buffer1);
			strcat(buf3, " ");
			strcat(buf3,buffer2);
			system(buf3);
			free(buf3);
			/* Réinitialisation de la variable set*/
			new_position.set = 0;

		}
	}
```

La fonction `setMouse()`va récupérer la valeur donnée par la variable `value` est en fonction de si l'évènement associé est X ou Y l'ajoute dans width ou height. Une fois qu'un nouveau couple de valeurs a été paramétré, la conversion est faite pour faire déplacer la souris à la position voulue sur l'écran.

```c
	/**
	* \fn print_events
	* \param fd descripteur
	* \return int
	* \brief Quand le programme est lancé, récupère chaque nouvel évènement lié au doigt
	*/
	int print_events(int fd){
		struct input_event ev[64];
		int i, rd;
		fd_set rdfs;

		FD_ZERO(&rdfs);
		FD_SET(fd, &rdfs);

		while (!stop) {
			select(fd + 1, &rdfs, NULL, NULL, NULL);
			if (stop)
				break;
			rd = read(fd, ev, sizeof(ev));

			/* Si problème avec la lecture des évènements */
			if (rd < (int) sizeof(struct input_event)) {
				printf("expected %d bytes, got %d\n",
					(int) sizeof(struct input_event), rd);
				perror("\nevtest: error reading");
				return 1;
			}

			/* Appel setMouse tant qu'il y a des évènements */
			for (i = 0; i < rd / sizeof(struct input_event); i++) {
				unsigned int type, code;

				type = ev[i].type;
				code = ev[i].code;
				setMouse(code,type,ev[i].value);
			}

		}

		ioctl(fd, EVIOCGRAB, (void*)0);
		return EXIT_SUCCESS;
	}
```
La fonction `print_events(int fd)` récupère la position à l'instant t du doigt sur le touchpad et envoie cette information à la fonction `setMouse()` pour gérer le déplacement de la souris sur l'écran.

```c
	/*!
	 * \fn do_capture
	 * \param device chemin pour accéder au fichier event du touchpad
	 * \return int
	 * \brief Vérifie si user a l'accès à ces données
	 */
	int do_capture(const char *device){
		int fd;
		char *filename = NULL;

		filename = strdup(device);
		if (!filename)
			return EXIT_FAILURE;

		/* Si user n'est pas sudo ou n'a pas accès au groupe input
			=> Termine le programme */
		if ((fd = open(filename, O_RDONLY)) < 0) {
			perror("error :");
			if (errno == EACCES && getuid() != 0)
				fprintf(stderr, "You do not have access to %s. Try "
						"running as root or tactos user instead.\n",
						filename);
			goto error;
		}

		if (!isatty(fileno(stdout)))
			setbuf(stdout, NULL);

		if (read_device_info(fd))
			goto error;

		/* Si l'utilisateur a les droits */
		printf("Absolute Mode activated ... (^C to exit)\n");

		signal(SIGINT, interrupt_handler);
		signal(SIGTERM, interrupt_handler);

		free(filename);

		return print_events(fd);

	error:
		free(filename);
		return EXIT_FAILURE;
	}

```

La fonction `do_capture()` vérifie l'identité de l'utilisateur pour voir s'il a les droits. Par défaut il faut compiler exécutable en sudo mais un script permet de créer un utilisateur n'ayant les droits que pour ce programme.

```c
	/* Main */
	int main(int argc, char **argv){
		new_position.set = 0;
		char* event = GetEvent();
		calibrage();
		char locate[20] = "/dev/input/event";
		strcat(locate,event);
		free(event);
		do_capture(strtok(locate,"\n"));
		return 0;

	}
```



* **Résultat**:
Le programme est efficace et fluide permettant une utilisation normale de l'ordinateur.


#### **5. Création d'un utilisateur pouvant utiliser evtest.c sans sudo**

Il a été demandé de faire une session utilisateur qui peut exécuter ce programme sans utiliser sudo.
Pour ce faire un script a été créé.

```sh
	#!/bin/sh
	VAR="$(printenv | grep -E "TACTOS=[[:digit:]]" | grep -E "[[:digit:]]")"
	echo "${VAR}"

	VAR2=tactos

	if [ -z $VAR ]; then

	  echo "Initialisation Tactos"

	  sudo adduser $VAR2
	  echo "création user tactos"

	  sudo usermod -a -G input $VAR2
	  echo "ajout de tactos dans le group input"

	else
	  echo "L'initialisation a déja été faite sur cet ordinateur"
	fi

	xhost +SI:localuser:$VAR2
```

Ce script doit être compilé à chaque démarrage de l'ordinateur. Si c'est la première fois que vous l'utilisez, il va créer un user tactos ayant les droits sur le groupe **input**, nécessaire pour utiliser ./evtest. La partie qui sera compilée à chaque fois est l'ajout de user dans .Xauthority pour permettre l'accès à tactos au serveur X sans changer de session.

Après l'exécution de ce script faite si c'est la première fois que vous avez compilé tactos:  
```sh    
sudo nano /etc/bash.bashrc
```
et rajoutez une ligne ```Export TACTOS=1``` puis relancez l'ordinateur.


## Points d'amélioration

* Gérer le multitouche
* Créer une interface graphique userfriendly
* Ne pas avoir à recompiler le script tactos à chaque utilisation du programme evtest.c


## Conclusion

<div style="text-align: justify">
Travailler pour cette TX m'a fait apprendre beaucoup de chose sur la programmation système et plus particulièrement le dialogue avec l'ordinateur et ses périphériques. De plus pouvoir travailler pour quelque chose qui va aider les personnes malvoyantes m'a motivé davantage. Je tenais à remercier Tobias Ollive qui m'a beaucoup aidé,guidé durant cette TX.
</div>

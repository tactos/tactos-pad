# Tactos-pad

Tactos-pad is an application to set pad from relative to absolute coordinate. It means that the pad is a direct mapping of the screen.
This application is based on [evtest](https://www.systutorials.com/docs/linux/man/1-evtest/).

## Building

### Dependencies

- meson (build system)
- X11

#### Compilation 

Tactos-pad uses [meson](https://mesonbuild.com/) as build system.

```bash
meson --buildtype=release hypra
cd hypra
ninja
sudo ./tactos-pad
```

#### Add your user to 'input' group (to avoid running as root)

```bash
sudo addgroup your_user input
reboot
```

#### Create a systemd service

You can create a systemd service to launch this program on boot by using the `tactos-pad.service` file. you just have to :

- change the `execstart` line to the path of your binary
- copy the file to `/usr/lib/systemd/user/` tu use this service as normal user
- reload systemd `systemctl deamon-reload`
- start and/or enable the service `systemctl --user enable/start tactos-pad`

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/). First version has been developped by Yann Ladeve.
